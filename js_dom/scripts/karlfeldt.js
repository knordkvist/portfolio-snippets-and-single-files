// Global variables
var GLOBAL = {
	// To keep track the open poem, if any
	openPoemId: null
}

window.onload = function(){
	prepareDocument();
}

function prepareDocument(){
	removeParas();
	// Retrieves all links which recide in a div that has the id "innehallsforteckning"
	var links = $$("div#innehallsforteckning a");
	// Sets each link's onclick function and then hides the link's corresponding poem
	for(var i = 0; i < links.length; i ++){
		links[i].onclick = function(){
			var poemId = getId(this.href);
			showPoem(poemId);
			return false;
		}
		var poemId = getId(links[i].href);
		addPoemInfo(poemId);
		hidePoem(poemId);
	}
}

// Removes all paragraphs with the class attribute "aterga"
function removeParas(){
	var paras = $$("p.aterga");
	for(var i = 0; i < paras.length; i ++){
		paras[i].remove();
	}
}

// Recieves a href-atttribute and returns an ID which belongs to a corresponding poem
function getId(href){
	return href.slice(href.indexOf("#") + 1);
}

// Recieves a poem ID and displays that poem. Hides an already open poem if one exists.
function showPoem(poemId){
	if(GLOBAL.openPoemId){
		hidePoem(GLOBAL.openPoemId);
	}
	var poem = $(poemId);
	GLOBAL.openPoemId = poemId;
	poem.show();
}

function hidePoem(poemId){
	var poem = $(poemId);
	poem.hide();
}

// Adds information about a poem plus a headline which can be clicked to toggle that information.
function addPoemInfo(poemId){
	var poemDiv = $(poemId);
	var header = document.createElement("h4");
	var infoPara = document.createElement("p");

	header.appendChild(document.createTextNode("Visa diktinformation"));
	header.onclick = function(){
		var infoPara = this.nextSibling;
		if(infoPara.visible()){
			infoPara.hide();
			this.firstChild.nodeValue = "Visa diktinformation";
		}
		else{
			infoPara.show();
			this.firstChild.nodeValue = "Dölj diktinformation";
		}
	}
	poemDiv.appendChild(header);

	// noParas = "The number of paragraphs in the div with the id poemId"
	var noParas = $$("div#" + poemId + " p").length;
	// noRows = "The number of line breaks which is a child element to a paragraph in the div with the id poemId"
	var noRows = $$("div#" + poemId + " p > br").length;
	var infoText = "Dikten har " + noParas + " stycken och består av " + noRows + " rader.";
	infoPara.appendChild(document.createTextNode(infoText));
	infoPara.hide();

	poemDiv.appendChild(infoPara);
}
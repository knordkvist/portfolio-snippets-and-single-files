/*
 * Almost equals function for floats,
 * based on the paper on
 * http://www.cygnus-software.com/papers/comparingfloats/comparingfloats.htm
 */
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
	// Make sure maxUlps is non-negative and small enough that the
	// default NAN won't compare as equal to anything.
	//assert(maxUlps > 0 && maxUlps < 4 * 1024 * 1024i);
	/*int x;
	printf("argc %d\n",argc);
	for (x=0; x<argc; x++)
	{
		printf("%s #%d\n",argv[x], x);
		printf("atof()  %f\n", (float)atof(argv[x]));
	}*/
	// Specifies the maximum number of units two floats can be apart,
	// but still be considered equal.
	int maxUlps = 10;
	float A = atof(argv[1]);
	int aInt = *(int*)&A;
	// Make aInt lexicographically ordered as a twos-complement int
	if (aInt < 0)
		aInt = 0x80000000 - aInt;
	int aIntPlus = aInt+1;
	int aIntMinus = aInt-1;

	printf("FloatA-1:\t%f\n", *(float*)&aIntMinus);
	printf("floatA:\t\t%f\n", *(float*)&aInt);
	printf("FloatA+1:\t%f\n\n", *(float*)&aIntPlus);

	float B = atof(argv[2]);
	int bInt = *(int*)&B;
	// Make bInt lexicographically ordered as a twos-complement int
	if (bInt < 0)
		bInt = 0x80000000 - bInt;
	int bIntPlus = bInt+1;
	int bIntMinus = bInt-1;

	printf("FloatB-1:\t%f\n", *(float*)&bIntMinus);
	printf("floatB:\t\t%f\n", *(float*)&bInt);
	printf("FloatB+1:\t%f\n\n", *(float*)&bIntPlus);
	printf("A as int:\t%d\n", aInt);
	printf("B as int:\t%d\n", bInt);

	int ret = 0;
	int intDiff = abs(aInt - bInt);
	if (intDiff <= maxUlps)
		ret = 1;
	if(ret)
		printf("The numbers were equal enough\n");
	else
		printf("The numbers were not equal enough\n");
	printf("End almostequals\n");
	return ret;
}


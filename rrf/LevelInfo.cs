using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.IsolatedStorage;
using System.IO;


namespace RampantRobotFactory
{
    class LevelInfo
    {
        public enum GameMode
        {
            None,
            Campaign,
            TimeAttack
        }

        int deathCount = 0;
        int level;
        TimeSpan totalTimePlayed = new TimeSpan(0, 0, 0, 0, 0);
        string fileName;
        string filePath;
        string directory;
        //TODO: Save target time in level file and load it here
        TimeSpan targetTime = new TimeSpan(0, 0, 1, 0, 0);
        int highScore = 0;
        Enum gameMode;
        TimeSpan[] timeAttackHighScores = new TimeSpan[10];
        string[] timeAttackHighScoreNames = new string[10];

        public LevelInfo(int level, Enum gameMode)
        {
            TimeSpan defaultHighScore = new TimeSpan(7,0,0,0,0);
            for (int i = 0; i < 10; i++)
            {
                timeAttackHighScores[i] = defaultHighScore;
                timeAttackHighScoreNames[i] = "RRF";
            }
            this.gameMode = gameMode;
            this.level = level;
            this.fileName = level + ".dat";
            this.directory = "LevelInfo";
            this.filePath = directory + "\\" + fileName;
        }

        public void calculateScore(TimeSpan elapsedTime, int robotDeaths)
        {
            int score = 7000;
            score -= robotDeaths * 1750;
            if (score < 0)
            {
                score = 0;
            }

            int timeBonus = 10000;
            double timeDifference = elapsedTime.TotalSeconds / this.targetTime.TotalSeconds;
            timeDifference = (1 - timeDifference);
            if (timeDifference < 0)
            {
                timeBonus += (int) (timeDifference * 40000);
                if (timeBonus < 0)
                {
                    timeBonus = 0;
                }
            }

            score += timeBonus;
            if(score > this.highScore)
            {
                this.highScore = score;
            }
        }

        public void loadInfo()
        {
            // Open up isolated storage
            using (IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication())
            {
                // see if our level info directory exists
                if (!storage.DirectoryExists(directory))
                {
                    storage.CreateDirectory(directory);
                }
                if (!storage.FileExists(filePath))
                {
                    return;
                }
                try
                {
                    using (IsolatedStorageFileStream stream = storage.OpenFile(filePath, FileMode.Open, FileAccess.Read))
                    {
                        using (BinaryReader reader = new BinaryReader(stream))
                        {
                            while (reader.BaseStream.Position < reader.BaseStream.Length)
                            {
                                // Read a line from our file
                                string line = reader.ReadString();

                                // If it isn't blank, we can do stuff with it
                                if (!string.IsNullOrEmpty(line))
                                {
                                    switch(line)
                                    {
                                        case "deathcount":
                                            this.deathCount = reader.ReadInt32();
                                            break;
                                        case "ttp":
                                            this.totalTimePlayed = new TimeSpan(reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32());
                                            break;
                                        case "highscore":
                                            this.highScore = reader.ReadInt32();
                                            break;
                                        case "ta_highscores":
                                            for (int i = 0; i < 10; i++)
                                            {
                                                timeAttackHighScores[i] = new TimeSpan(reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32(), reader.ReadInt32());
                                            }
                                            break;
                                        case "ta_highscorenames":
                                            for (int i = 0; i < 10; i++)
                                            {
                                                timeAttackHighScoreNames[i] = reader.ReadString();
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception e)
                {
                    if (!(e is FileNotFoundException))
                    {
                        // The level file might be corrupt, get rid of it
                        storage.DeleteFile(filePath);
                    }
                }
            }
        }

        public void saveInfo()
        {
            try
            {
                // Open up isolated storage
                using (IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication())
                {
                    // If our level info directory doesn't exist, then we create a new directory
                    if (!storage.DirectoryExists(directory))
                    {
                        storage.CreateDirectory(directory);
                    }

                    // Create a file we'll use to store the level info
                    using (IsolatedStorageFileStream stream = storage.OpenFile(filePath, FileMode.Create))
                    {
                        using (BinaryWriter writer = new BinaryWriter(stream))
                        {
                            writer.Write("ttp");
                            writer.Write(this.totalTimePlayed.Days);
                            writer.Write(this.totalTimePlayed.Hours);
                            writer.Write(this.totalTimePlayed.Minutes);
                            writer.Write(this.totalTimePlayed.Seconds);
                            writer.Write(this.totalTimePlayed.Milliseconds);

                            writer.Write("deathcount");
                            writer.Write(deathCount);
                            if(gameMode.Equals(GameMode.Campaign))
                            {
                                writer.Write("highscore");
                                writer.Write(highScore);
                            }
                            else if (gameMode.Equals(GameMode.TimeAttack))
                            {
                                writer.Write("ta_highscores");
                                for (int i = 0; i < 10; i++)
                                {
                                    writer.Write(timeAttackHighScores[i].Days);
                                    writer.Write(timeAttackHighScores[i].Hours);
                                    writer.Write(timeAttackHighScores[i].Minutes);
                                    writer.Write(timeAttackHighScores[i].Seconds);
                                    writer.Write(timeAttackHighScores[i].Milliseconds);
                                }

                                writer.Write("ta_highscorenames");
                                for (int i = 0; i < 10; i++)
                                {
                                    writer.Write(timeAttackHighScoreNames[i]);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                System.Diagnostics.Debug.WriteLine(this.filePath);
            }
        }

        public void updateTimeAttackHighScores(TimeSpan completeTime, String name)
        {
            TimeSpan compareTime = completeTime;
            String compareName = name;
            
            for (int i = 0; i < 10; i++)
            {
                if (this.timeAttackHighScores[i].CompareTo(compareTime) > 0)
                {
                    TimeSpan tmpTime = this.timeAttackHighScores[i];
                    String tmpName = this.timeAttackHighScoreNames[i];
                    this.timeAttackHighScores[i] = compareTime;
                    this.timeAttackHighScoreNames[i] = compareName;
                    compareTime = tmpTime;
                    compareName = tmpName;
                }
            }
        }

        public TimeSpan TotalTimePlayed 
        {

            get { return totalTimePlayed; }
            set { totalTimePlayed = value;}
        }
        public int DeathCount
        { 
            get { return deathCount; }
            set { deathCount = value; }
        }

        public int HighScore
        {
            get { return highScore; }
            set { highScore = value; }
        }

        public TimeSpan[] TimeAttackHighScores
        {
            get { return timeAttackHighScores; }
            set { timeAttackHighScores = value; }
        }

        public string[] TimeAttackHighScoreNames
        {
            get { return timeAttackHighScoreNames; }
            set { TimeAttackHighScoreNames = value; }
        }
    }
}
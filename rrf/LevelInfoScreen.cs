using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace RampantRobotFactory
{
    class LevelInfoScreen : MenuScreen
    {
        Enum gameMode;
        int level;
        LevelInfo levelInfo;
        MenuEntry play = new MenuEntry("Play Level", false);
        MenuEntry highScore = new MenuEntry("Highscore", false);
        MenuEntry back = new MenuEntry("Back", false);

        public LevelInfoScreen(string title, int level, Enum gameMode)
            :base(String.Empty)
        {
            this.gameMode = gameMode;
            this.level = level;
            this.levelInfo = new LevelInfo(level, gameMode);
            levelInfo.loadInfo(); 

            play.Selected += PlayMenuEntrySelected;
            highScore.Selected += HighScoreMenuEntrySelected;
            back.Selected += BackMenuEntrySelected;
            
            menuEntries.Add(play);
            if(gameMode.Equals(LevelInfo.GameMode.TimeAttack))
            {
                menuEntries.Add(highScore);
            }
            menuEntries.Add(back);
        }

        void PlayMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            LoadingScreen.Load(ScreenManager, true, e.PlayerIndex, new GameplayScreen(ScreenManager, level, gameMode));
        }

        void HighScoreMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            ScreenManager.AddScreen(new HighScoreScreen(level, levelInfo.TimeAttackHighScores, levelInfo.TimeAttackHighScoreNames), e.PlayerIndex);
        }

        void BackMenuEntrySelected(object sender, PlayerIndexEventArgs e)
        {
            this.ExitScreen();
        }

        public override void Draw(GameTime gameTime)
        {
 	        base.Draw(gameTime);
        }

        public override void UnloadContent()
        {
            base.UnloadContent();
        }
    }
}

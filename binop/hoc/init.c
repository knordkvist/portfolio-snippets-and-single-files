#include "hoc.h"
#include "y.tab.h"
#include <math.h>

static struct {		/* Keywords */
	char	*name;
	int	kval;
} keywords[] = {
	{"proc",	PROC},
	{"func",	FUNC},
	{"return",	RETURN},
	{"if",		IF},
	{"else",	ELSE},
	{"while",	WHILE},
	{"for",		FOR},
	{"print",	PRINT},
	{"read",	READ},
	{"logstart",LOGSTART},	/* Start logging session */
	{"logstop", LOGSTOP},	/* Stop logging session  */
	{"help",	HELP},
	{"quit",	QUIT},
	{0,			0}
};

static struct {		/* Constants */
	char *name;
	double cval;
} consts[] = {
	{"PI",		 3.14159265358979323846},
	{"E",		 2.71828182845904523536},
	{"GAMMA",	 0.57721566490153286060},  /* Euler */
	{"DEG",		57.29577951308232087680},  /* deg/radian */
	{"PHI",		 1.61803398874989484820},  /* golden ratio */
	{"PREC",	15},	/* output precision */
	{0,	 		 0}
};

static struct {		/* Built-ins */
	char *name;
	double	(*func)(double);
} builtins[] = {
	{"sin",	sin},
	{"cos",	cos},
	{"tan",	tan},
	{"atan", atan},
	{"asin", Asin},   /* checks range */
	{"acos", Acos},   /* checks range */
	{"sinh", Sinh},   /* checks range */
	{"cosh", Cosh},	  /* checks range */
	{"tanh", tanh},
	{"log", Log},     /* checks range */
	{"log10", Log10}, /* checks range */
	{"exp", Exp},     /* checks range */
	{"sqrt", Sqrt},	  /* checks range */
	{"gamma", Gamma}, /* checks range */
	{"fac", factorial},
	{"int", integer},
	{"abs", fabs},
	{"erf", erf},
	{"erfc", erfc},
	{"fib",fibonacci},
	{0,	0}
};

static struct {         /* Built-ins that take one integer argument*/
        char *name;
        int     (*func)(int);
} builtins_int[] = {
        {"exit",Exit},
        {0,0}
};


static struct {		/* Built-ins that take multiple arguments*/
	char *name;
	double	(*func)(int, double*);
} builtins_multi[] = {
	{"rand",Rand},
	{"srand",Srand},
	{"min",min},
	{"max",max},
	{"gcd",gcd},
	{"avg",average},
	{"binop",binop},
	{0,0}
};

void
init(void)	/* install constants and built-ins in table */
{
	int i;
	Symbol *s;
	for (i = 0; keywords[i].name; i++) {
		install(keywords[i].name, keywords[i].kval, 0.0);
	}
	for (i = 0; consts[i].name; i++) {
		install(consts[i].name, VAR, consts[i].cval);
	}
	for (i = 0; builtins[i].name; i++) {
		s = install(builtins[i].name, BLTIN, 0.0);
		s->u.ptr = builtins[i].func;
	}
        for (i = 0; builtins_int[i].name; i++) {
                s = install(builtins_int[i].name, BLTIN_INT, 0.0);
                s->u.ptr_int = builtins_int[i].func;
        }
	for (i = 0; builtins_multi[i].name; i++) {
		s = install(builtins_multi[i].name, BLTIN_MULTI, 0.0);
		s->u.ptr_multi = builtins_multi[i].func;
	}
}

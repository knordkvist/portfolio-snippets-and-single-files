/**
* \file:     hoc.h
* \brief:    Header for the calculator 'hoc'.
*            Laboration L12 LinDE, Linux Development Environment
*            Umea University, Sweden.
* \date:     $Date: 2012-01-23 18:23:04 +0100 (Mon, 23 Jan 2012) $ $LastChangedBy: krno0010@student.umu.se $
* \Revision: $Revision: 28 $
*/

typedef void (*Inst)(void);
#define	STOP	(Inst) 0


typedef struct Symbol {	/* symbol table entry */
	char	*name;
	long	type;
	union {
		double	val;		/* VAR */
		double	(*ptr)(double);	/* BLTIN */
		int	(*ptr_int)(int); /* BLTIN_INT */
		double	(*ptr_multi)(int,double*);	/* BLTIN_MULTI */
		Inst	*defn;		/* FUNCTION, PROCEDURE */
		char	*str;		/* STRING */
	} u;
	struct Symbol	*next;	/* to link to another */
} Symbol;
Symbol	*install(char*, int, double), *lookup(char*);

typedef union Datum {	/* interpreter stack type */
	double	val;
	Symbol	*sym;
} Datum;

extern	double Fgetd(int);
extern	int moreinput(void);
extern	void define(Symbol*), verify(Symbol*);
extern	Datum pop(void);
extern	void initcode(void), push(Datum), xpop(void), constpush(void);
extern	void varpush(void);
extern	void eval(void), add(void), sub(void), mul(void), divop(void), mod(void);
extern	void negate(void), power(void);
extern	void addeq(void), subeq(void), muleq(void), diveq(void), modeq(void);

extern	Inst *progp, *progbase, prog[], *code(Inst);
				         extern	void assign(void), bltin(void), bltin_int(void), bltin_multi(void), varread(void);
extern	void prexpr(void), prstr(void);
extern	void gt(void), lt(void), eq(void), ge(void), le(void), ne(void);
extern	void and(void), or(void), not(void);
extern	void ifcode(void), whilecode(void), forcode(void);
extern	void call(void), arg(void), argassign(void);
extern	void funcret(void), procret(void);
extern	void preinc(void), predec(void), postinc(void), postdec(void);
extern	void argaddeq(void), argsubeq(void), argmuleq(void);
extern	void argdiveq(void), argmodeq(void);
extern	void execute(Inst*);
extern	void printtop(void);

extern double	Log(double), Log10(double), Gamma(double), Sqrt(double), Exp(double);
extern double	Asin(double), Acos(double), Sinh(double), Cosh(double), integer(double);
extern double	Pow(double, double);

extern double	Rand(int, double*), Srand(int,double*);

extern double	factorial(double);
extern double	min(int, double*);
extern double	max(int, double*);
extern double	gcd(int, double*);
extern double	average(int, double*); // Ola
extern double   fibonacci(double); // Ola
extern double	binop(int, double*);

extern	void init(void);
extern	int yyparse(void);
extern	int yylex(void);
extern	void yyerror(char*);
extern	void execerror(char*, char*);
extern	void *emalloc(unsigned);

extern	void logstart(char*);
extern	void logstop(void);
extern	void prend(void);

extern	void help(char*);

extern	void defnonly(char *);
extern	void warning(char *s, char *t);

extern	int Exit(int);

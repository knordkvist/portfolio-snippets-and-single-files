#include <math.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>

extern	int	errno;
double	errcheck();

#include "hoc.h"

double	errcheck(double, char*);

double
Log(double x)
{
	return errcheck(log(x), "log");
}
double
Log10(double x)
{
	return errcheck(log10(x), "log10");
}

double
Sqrt(double x)
{
	return errcheck(sqrt(x), "sqrt");
}


double
Gamma(double x)
{
	double y;
	y=errcheck(tgamma(x), "gamma");
	if(y>88.0)
		execerror("gamma result out of range", (char *)0);
	return y;
}


double
Exp(double x)
{
	return errcheck(exp(x), "exp");
}
double
Asin(double x)
{
	return errcheck(asin(x), "asin");
}

double
Acos(double x)
{
	return errcheck(acos(x), "acos");
}

double
Sinh(double x)
{
	return errcheck(sinh(x), "sinh");
}

double
Cosh(double x)
{
	return errcheck(cosh(x), "cosh");
}
double
Pow(double x, double y)
{
	return errcheck(pow(x,y), "exponentiation");
}

double
integer(double x)
{
	return (double)(long)x;
}

double
errcheck(double d, char* s)	/* check result of library call */
{
	if (errno == EDOM) {
		errno = 0;
		execerror(s, "argument out of domain");
	} else if (errno == ERANGE) {
		errno = 0;
		execerror(s, "result out of range");
	}
	return d;
}


/**
 * @brief Generate a random number
 * 
 * Generates a random number 
 * argc=0:
 * 	generates a number between 0 and 1.0
 * argc=1:
 *  generates a number between 0 and args[0]
 * argc=2:
 *  generates a number between args[0] and args[1]
 *
 * @param [in] argc The size of the args array
 * @param [in] args[] An array of double:s
 *
 * @return a random number, or 0 on error
 */
double
Rand(int argc, double args[])
{
	double r = rand()/(double)RAND_MAX;
	if(argc==0) {
		return r;
	} else if(argc==1) {
		return r*args[0];
	} else if(argc==2) {
		return fabs(args[0]-args[1])*r + fmin(args[0],args[1]);
	} else {
		execerror("rand", "Too many arguments");
		return 0;
	}
}


/**
 * @brief Seeds the random number generator
 * 
 * Seeds the random number generator
 * argc=0:
 * 	seeds with the current time
 * argc=1:
 *  seeds with the value of args[0]
 *
 * @param argc The size of the args array
 * @param args[] An array of double:s
 *
 * @return The seed that was used
 */
double
Srand(int argc, double args[])
{
	unsigned int seed;

	if(argc==0) {
		seed=time(NULL);
	} else if(argc==1) {
		seed=(int)args[0];
	} else {
		execerror("srand", "Too many arguments");
		return -1;
	}

	srand(seed);
	return seed;
}


/**
 * @brief Find the factorial number in position given
 * 
 * Gives back the factorial number that corresponds to the given number.
 * If the user for example gives 3 then the number 6 is returned.
 * 
 * @param high the highest number in the factorial sequence
 *
 * @return the result of the factorial multiplication
 */
double
factorial(double high)
{
	if (high-round(high)) {
		execerror("factorial", "Only whole numbers have factorials");
		return -1;
	}
	
	int n;
	int sum=1;
	if (high!=0) {
		for (n=abs(high); n!=1; n--){
			sum*=n;
		}
	}
	return sum;
}



/**
 * @brief Find the smallest number between given values
 * 
 * Gives back the smallest number between given values. An arbitrary number
 * of numbers can be passed. Even negative numbers work. At least two numbers
 * should be given
 * 
 * @param number the first number
 * @param number the second number
 * .....
 *
 * @return min the smallest number of the ones above
 */
double
min(int argc, double args[])
{
	double min=INFINITY;
	
	if(argc<2) {
		execerror("min","Too few arguments");
		return -1;
	} else {
		int i;
		for (i=0; i<argc; i++){
			if (args[i]<min) {
				min=args[i];
			}
		}	
	}

	return min;
}

/**
 * @brief Find the biggest number between given values
 * 
 * Gives back the biggest number between given values. An arbitrary number
 * of numbers can be passed. Even negative numbers work. At least two numbers
 * should be given
 * 
 * @param number the first number
 * @param number the second number
 * .....
 *
 * @return min the biggest number of the ones above
 */
double
max(int argc, double args[])
{
	double max=-INFINITY;
	
	if(argc<2) {
		execerror("max","Too few arguments");
		return -1;
	} else {
		int i;
		for (i=0; i<argc; i++){
			if (args[i]>max) {
				max=args[i];
			}
		}	
	}

	return max;
}


/**
 * @brief Find the greatest common divisor between two numbers
 * 
 * Gives back the greatest common divisor between the numbers given.
 * Only two numbers can be given.
 * 
 * @param number1 the first number
 * @param number2 the second number
 *
 * @return number the greatest common divisor. -1 is returned
 * on error
 */
double
gcd(int argc, double args[])
{
	if(argc!=2) {
		execerror("gcd","Only two arguments acceptable");
		return -1;
	}
	

	
	if (args[0]-round(args[0])+args[1]-round(args[1])) {
		execerror("gcd", "Only whole numbers accepted as arguments");
		return -1;
	}
	
	int a, b, cd;
	a=(int)args[0];
	b=(int)args[1];
	
	if(argc==2) {

	    while(1)
	    {
			cd = a%b;
			if(cd==0)
				return (double)b;
			a = b;
			b = cd;
	    }
	}
	
	return -1;
}



/**
 * @brief Return the n-th number in the Fibonacci sequence
 * 
 * Returns the n-th number in the Fibonacci sequence. A Fibonacci
 * progression is an arithmetic progression where each next element
 * equals to the sum of the two previous ones. The numeration of
 * elements starts from 1.
 *
 * n<=0:
 * 	returns 0
 * n=1:
 *      returns 0; The 1st element is equal to 0 by definition.
 * n=2:
 *      returns 1; The 2nd element is equal to 1 by definition.
 * n>=3:
 *      returns the sum of the two previous elements;
 *
 * @param [in] n The ordinal number of the element in the Fibonacci row
 *
 * @return the n-th Fibonacci number 
 */
double 
fibonacci(double d) {
  int n = floor(d);
  // if 0 or below is passed, the result is 0
  if (n<=0) {
    return 0;
  }
  // the 1st element is 0 by definition
  else if (n==1) {
    return 0;
  }
  // the 2nd element is 1 by definition
  else if (n==2) {
    return 1;
  }
  // if we got that far, the ordinal number in the Fibonacci sequence
  // is at least 3
  int i=0;
  double prev_one=0.0L; // 1st previous element
  double prev_two=1.0L; // 2nd previous element
  double current_number;
  // reiterating the Fibonacci sequence until the n-th element is reached
  for (i=3; i<=n; i++) {
    current_number=prev_one+prev_two; // the current element is the sum of the two previous
    prev_one = prev_two;       // 2nd previous becomes 1st previous
    prev_two = current_number; // current becomes 2nd previous
  }
  return current_number;
 } 



/**
 * @brief Returns the average of the given values 
 * 
 * Calculate the average value of a array with doubles.
 * This function can handle an arbitrary number of values.
 *
 * @param count, how many ellements there are in the list.
 * @param args, a list of the values.
 * 
 *
 * @return The average of a list of values.
 */
double 
average(int count, double args[])
{
	double sum;
	int i;
	
	if (count < 1){
		execerror("average","Must have at least one value");
		return -1;
	}
	for( i = 0; i < count; i++){
		sum += args[i]/count;
	}
	return sum;
}

/**
 * @brief Calculates the probability that an event with two outcomes and the
 * probability p will occur r times in n trials.
 *
 * In a binomial experiment there are two mutually exclusive outcomes,
 * usually thought of as either "success" or "failure".
 * Two examples are flipping a coin (heads is success and tails is failure, or
 * the other way around) and rolling a die (5 is success and everything else is
 * failure).
 *
 * When computing binomial probability you need to know the probability that
 * the event will occur (p), the number of trials (n) and the number of times
 * you wish the event to occur (r).
 * 
 * @param argc	The size of args.
 *
 * @param args	Contains the three values needed to calculate the probability.
 *		The first element represent the probability that
 * 		the event will occur, the second element represent the number
 *		of trials that will be performed and the third element
 *		represent the number of succesfull trials.
 *
 * @return	The probability that the event will occur.
 */
double binop(int argc, double args[]) {
	if (argc != 3) {
		execerror("binop","Takes 3 arguments");
		return -1;
	}
	// Make sure all values are positive.
	for (int i=0; i<argc; ++i) {
		if (args[i] < 0) {
			execerror("binop", "Arguments cannot contain negative "
					"values");
			return -1;
		}
	}
	double p = args[2];
	double n = args[1];
	double r = args[0];

	if (n < 1) {
		execerror("binop","Number of trials need to be greater than 0");
		return -1;
	}

	if(p > 1) {
		execerror("binop", "Probability must be between 0 and 1");
		return -1;

	}
	// Numbers of trials and successfull events need to be whole numbers.
	if (n != round(n) || r != round(r)) {
		execerror("binop", "Trials and successfull events need to be "
				"whole numbers");
		return -1;
	}
	double probability = 0;
	double combinations = 1;
	/*
	 * The number of possible combinations using r elements
	 * in a set with n elements can be calculated using the following:
	 *
	 * ( n )   (n-0)   (n-1)   (n-2)         (n-(r-1))
	 * (   ) = ----- x ----- x ----- x ... x ---------
	 * ( r )     1       2       3               r
	 */
	for(int i=0; i<=r-1; ++i) {
		combinations *= (n-i) / (i+1);
	}
	probability = combinations * Pow(p,r) * Pow(1-p, n-r);
	return probability;
}

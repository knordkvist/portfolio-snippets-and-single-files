/**
* \file:     code.c
* \brief:    Code for features implemented in the 'hoc' calculator.
*            Laboration L12 LinDE, Linux Development Environment
*            Umea University, Sweden.
* \details:  Each function or command in hoc is typically implemented
*            here as C code. The steps for adding a new command/function
*            in hoc is usually:
*            1. Declare a new token in hoc.h (use upper-case)
*            2. Map the function/command to the token in init.c
*            3. In hoc.y, declare the grammar(s) and correcponding action.
*               If global entities are used, define and initialize them here.
*            4. Here in code.c, define the corresponding C function(s)
*            5. Run the "make" command. Take action on warnings and errors.
*            6. Update the man pages in hoc.1 according to your changes,
*               check it with "man ./hoc.1" carefully.
*            7. Commit your changes to the repository (svn commit -m "Comment"
* \date:     $Date: 2012-01-22 15:51:21 +0100 (Sun, 22 Jan 2012) $ $LastChangedBy: ubbe00@gmail.com $
* \Revision: $Revision: 27 $
*/

#include "hoc.h"
#include "y.tab.h"
#include <stdio.h>
#include <stdlib.h>

#define	NSTACK	256

static Datum stack[NSTACK];	/* the stack */
static Datum *stackp;		/* next free spot on stack */

#define	NPROG	2000
Inst	prog[NPROG];		/* the machine */
Inst	*progp;			/* next free spot for code generation */
Inst	*pc;			/* program counter during execution */
Inst	*progbase = prog;	/* start of current subprogram */
int	returning;		/* 1 if return stmt seen */
extern int	indef;		/* 1 if parsing a func or proc */
FILE	*logfp;			/* Log output file ptr */

typedef struct Frame {		/* proc/func call stack frame */
	Symbol	*sp;		/* symbol table entry */
	Inst	*retpc;		/* where to resume after return */
	Datum	*argn;		/* n-th argument on stack */
	int	nargs;		/* number of arguments */
} Frame;
#define	NFRAME	100
Frame	frame[NFRAME];
Frame	*fp;			/* frame pointer */

void
initcode(void)
{
	progp = progbase;
	stackp = stack;
	fp = frame;
	returning = 0;
	indef = 0;
}

void
push(Datum d)
{
	if (stackp >= &stack[NSTACK])
		execerror("stack too deep", 0);
	*stackp++ = d;
}

Datum
pop(void)
{
	if (stackp == stack)
		execerror("stack underflow", 0);
	return *--stackp;
}

void
xpop(void)	/* for when no value is wanted */
{
	if (stackp == stack)
		execerror("stack underflow", (char *)0);
	--stackp;
}

void
constpush(void)
{
	Datum d;
	d.val = ((Symbol *)*pc++)->u.val;
	push(d);
}

void
varpush(void)
{
	Datum d;
	d.sym = (Symbol *)(*pc++);
	push(d);
}

void
whilecode(void)
{
	Datum d;
	Inst *savepc = pc;

	execute(savepc+2);	/* condition */
	d = pop();
	while (d.val) {
		execute(*((Inst **)(savepc)));	/* body */
		if (returning)
			break;
		execute(savepc+2);	/* condition */
		d = pop();
	}
	if (!returning)
		pc = *((Inst **)(savepc+1)); /* next stmt */
}

void
forcode(void)
{
	Datum d;
	Inst *savepc = pc;

	execute(savepc+4);		/* precharge */
	pop();
	execute(*((Inst **)(savepc)));	/* condition */
	d = pop();
	while (d.val) {
		execute(*((Inst **)(savepc+2)));	/* body */
		if (returning)
			break;
		execute(*((Inst **)(savepc+1)));	/* post loop */
		pop();
		execute(*((Inst **)(savepc)));	/* condition */
		d = pop();
	}
	if (!returning)
		pc = *((Inst **)(savepc+3)); /* next stmt */
}

void
ifcode(void) 
{
	Datum d;
	Inst *savepc = pc;	/* then part */

	execute(savepc+3);	/* condition */
	d = pop();
	if (d.val)
		execute(*((Inst **)(savepc)));	
	else if (*((Inst **)(savepc+1))) /* else part? */
		execute(*((Inst **)(savepc+1)));
	if (!returning)
		pc = *((Inst **)(savepc+2)); /* next stmt */
}

void
define(Symbol* sp)	/* put func/proc in symbol table */
{
	sp->u.defn = progbase;	/* start of code */
	progbase = progp;	/* next code starts here */
}

void
call(void) 		/* call a function */
{
	Symbol *sp = (Symbol *)pc[0]; /* symbol table entry */
				      /* for function */
	if (fp++ >= &frame[NFRAME-1])
		execerror(sp->name, "call nested too deeply");
	fp->sp = sp;
	fp->nargs = (long int)pc[1];
	fp->retpc = pc + 2;
	fp->argn = stackp - 1;	/* last argument */
	execute(sp->u.defn);
	returning = 0;
}

static void
ret(void) 		/* common return from func or proc */
{
	int i;
	for (i = 0; i < fp->nargs; i++)
		pop();	/* pop arguments */
	pc = (Inst *)fp->retpc;
	--fp;
	returning = 1;
}

void
funcret(void) 	/* return from a function */
{
	Datum d;
	if (fp->sp->type == PROCEDURE)
		execerror(fp->sp->name, "(proc) returns value");
	d = pop();	/* preserve function return value */
	ret();
	push(d);
}

void
procret(void) 	/* return from a procedure */
{
	if (fp->sp->type == FUNCTION)
		execerror(fp->sp->name,
			"(func) returns no value");
	ret();
}

double*
getarg(void) 	/* return pointer to argument */
{
	int nargs = (long int) *pc++;
	if (nargs > fp->nargs)
	    execerror(fp->sp->name, "not enough arguments");
	return &fp->argn[nargs - fp->nargs].val;
}

void
arg(void) 	/* push argument onto stack */
{
	Datum d;
	d.val = *getarg();
	push(d);
}

void
argassign(void) 	/* store top of stack in argument */
{
	Datum d;
	d = pop();
	push(d);	/* leave value on stack */
	*getarg() = d.val;
}

void
argaddeq(void) 	/* store top of stack in argument */
{
	Datum d;
	d = pop();
	d.val = *getarg() += d.val;
	push(d);	/* leave value on stack */
}

void
argsubeq(void) 	/* store top of stack in argument */
{
	Datum d;
	d = pop();
	d.val = *getarg() -= d.val;
	push(d);	/* leave value on stack */
}

void
argmuleq(void) 	/* store top of stack in argument */
{
	Datum d;
	d = pop();
	d.val = *getarg() *= d.val;
	push(d);	/* leave value on stack */
}

void
argdiveq(void) 	/* store top of stack in argument */
{
	Datum d;
	d = pop();
	d.val = *getarg() /= d.val;
	push(d);	/* leave value on stack */
}

void
argmodeq(void) 	/* store top of stack in argument */
{
	Datum d;
	double *x;
	long y;
	d = pop();
	/* d.val = *getarg() %= d.val; */
	x = getarg();
	y = *x;
	d.val = *x = y % (long) d.val;
	push(d);	/* leave value on stack */
}

void
bltin(void) 
{

	Datum d;
	d = pop();
	d.val = (*(double (*)(double))*pc++)(d.val);
	push(d);
}

void
bltin_int(void)
{
        Datum d;
        d = pop();
        d.val = (*(int (*)(int))*pc++)(d.val);
        push(d);

}

/**
 * @brief Like bltin(), but can handle any number of arguments to the function
 */
void
bltin_multi(void) 
{
	Datum d;

	/*Get number of arguments*/
	int narg = (long int)pc[1];

	/*Make room for the arguments*/
	double args[narg];

	/*Get arguments from stack*/
	int i;
	for(i=0;i<narg;i++) {
		d=pop();
		args[i]=d.val;
	}

	/*Call function*/
	d.val = (*(double (*)(int, double*))*pc)(narg,args);

	/*Increment program counter*/
	pc+=2;

	/*Return result*/
	push(d);
}

void
add(void)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val += d2.val;
	push(d1);
}

void
sub(void)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val -= d2.val;
	push(d1);
}

void
mul(void)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val *= d2.val;
	push(d1);
}

void
divop(void)
{
	Datum d1, d2;
	d2 = pop();
	if (d2.val == 0.0)
		execerror("division by zero", (char *)0);
	d1 = pop();
	d1.val /= d2.val;
	push(d1);
}

void
mod(void)
{
	Datum d1, d2;
	long x;
	d2 = pop();
	if (d2.val == 0.0)
		execerror("division by zero", (char *)0);
	d1 = pop();
	/* d1.val %= d2.val; */
	x = d1.val;
	x %= (long) d2.val;
	d1.val = d2.val = x;
	push(d1);
}

void
negate(void)
{
	Datum d;
	d = pop();
	d.val = -d.val;
	push(d);
}

void
verify(Symbol* s)
{
	if (s->type != VAR && s->type != UNDEF)
		execerror("attempt to evaluate non-variable", s->name);
	if (s->type == UNDEF)
		execerror("undefined variable", s->name);
	fflush(stderr);
}

void
eval(void)		/* evaluate variable on stack */
{
	Datum d;
	d = pop();
	verify(d.sym);
	d.val = d.sym->u.val;
	push(d);
}

void
preinc(void)
{
	Datum d;
	d.sym = (Symbol *)(*pc++);
	verify(d.sym);
	d.val = d.sym->u.val += 1.0;
	push(d);
}

void
predec(void)
{
	Datum d;
	d.sym = (Symbol *)(*pc++);
	verify(d.sym);
	d.val = d.sym->u.val -= 1.0;
	push(d);
}

void
postinc(void)
{
	Datum d;
	double v;
	d.sym = (Symbol *)(*pc++);
	verify(d.sym);
	v = d.sym->u.val;
	d.sym->u.val += 1.0;
	d.val = v;
	push(d);
}

void
postdec(void)
{
	Datum d;
	double v;
	d.sym = (Symbol *)(*pc++);
	verify(d.sym);
	v = d.sym->u.val;
	d.sym->u.val -= 1.0;
	d.val = v;
	push(d);
}

void
gt(void)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val = (double)(d1.val > d2.val);
	push(d1);
}

void
lt(void)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val = (double)(d1.val < d2.val);
	push(d1);
}

void
ge(void)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val = (double)(d1.val >= d2.val);
	push(d1);
}

void
le(void)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val = (double)(d1.val <= d2.val);
	push(d1);
}

void
eq(void)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val = (double)(d1.val == d2.val);
	push(d1);
}

void
ne(void)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val = (double)(d1.val != d2.val);
	push(d1);
}

void
and(void)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val = (double)(d1.val != 0.0 && d2.val != 0.0);
	push(d1);
}

void
or(void)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val = (double)(d1.val != 0.0 || d2.val != 0.0);
	push(d1);
}

void
not(void)
{
	Datum d;
	d = pop();
	d.val = (double)(d.val == 0.0);
	push(d);
}

void
power(void)
{
	Datum d1, d2;
	d2 = pop();
	d1 = pop();
	d1.val = Pow(d1.val, d2.val);
	push(d1);
}

void
assign(void)
{
	Datum d1, d2;
	d1 = pop();
	d2 = pop();
	if (d1.sym->type != VAR && d1.sym->type != UNDEF)
		execerror("assignment to non-variable",
			d1.sym->name);
	d1.sym->u.val = d2.val;
	d1.sym->type = VAR;
	push(d2);
}

void
addeq(void)
{
	Datum d1, d2;
	d1 = pop();
	d2 = pop();
	if (d1.sym->type != VAR && d1.sym->type != UNDEF)
		execerror("assignment to non-variable",
			d1.sym->name);
	d2.val = d1.sym->u.val += d2.val;
	d1.sym->type = VAR;
	push(d2);
}

void
subeq(void)
{
	Datum d1, d2;
	d1 = pop();
	d2 = pop();
	if (d1.sym->type != VAR && d1.sym->type != UNDEF)
		execerror("assignment to non-variable",
			d1.sym->name);
	d2.val = d1.sym->u.val -= d2.val;
	d1.sym->type = VAR;
	push(d2);
}

void
muleq(void)
{
	Datum d1, d2;
	d1 = pop();
	d2 = pop();
	if (d1.sym->type != VAR && d1.sym->type != UNDEF)
		execerror("assignment to non-variable",
			d1.sym->name);
	d2.val = d1.sym->u.val *= d2.val;
	d1.sym->type = VAR;
	push(d2);
}

void
diveq(void)
{
	Datum d1, d2;
	d1 = pop();
	d2 = pop();
	if (d1.sym->type != VAR && d1.sym->type != UNDEF)
		execerror("assignment to non-variable",
			d1.sym->name);
	d2.val = d1.sym->u.val /= d2.val;
	d1.sym->type = VAR;
	push(d2);
}

void
modeq(void)
{
	Datum d1, d2;
	long x;
	d1 = pop();
	d2 = pop();
	if (d1.sym->type != VAR && d1.sym->type != UNDEF)
		execerror("assignment to non-variable",
			d1.sym->name);
	/* d2.val = d1.sym->u.val %= d2.val; */
	x = d1.sym->u.val;
	x %= (long) d2.val;
	d2.val = d1.sym->u.val = x;
	d1.sym->type = VAR;
	push(d2);
}

void
printtop(void)	/* pop top value from stack, print it */
{
	Datum d;
	static Symbol *s;	/* last value computed */
	if (s == 0)
		s = install("_", VAR, 0.0);
	d = pop();
	printf("%.*g\n", (int)lookup("PREC")->u.val, d.val);
	if (logfp) {
		fprintf(logfp, "%.*g\n", (int)lookup("PREC")->u.val, d.val);
		fflush(logfp);
	}
	s->u.val = d.val;
}

void
prexpr(void)	/* print numeric value */
{
	Datum d;
	d = pop();
	printf("%.*g ", (int)lookup("PREC")->u.val, d.val);
	if (logfp) {
		fprintf(logfp, "%.*g ", (int)lookup("PREC")->u.val, d.val);
		fflush(logfp);
	}
	fflush(stdout);
	fflush(stderr);
}

void
prstr(void)		/* print string value */ 
{
	char	*currpc;

	currpc = (char *) *pc++;
	printf("%s", currpc);
	if (logfp) {
		fprintf(logfp, "%s", currpc);
		fflush(logfp);
	}
	fflush(stdout);
	fflush(stderr);
}

void
prend(void)	/* prints end of line */
{
	if (logfp) {
		fprintf(logfp, "\n");
		fflush(logfp);
	}
}

void
varread(void)	/* read into variable */
{
	Datum d;
	extern FILE *fin;
	Symbol *var = (Symbol *) *pc++;
  Again:
	switch (fscanf(fin, "%lf", &var->u.val)) {
	case EOF:
		if (moreinput())
			goto Again;
		d.val = var->u.val = 0.0;
		break;
	case 0:
		execerror("non-number read into", var->name);
		break;
	default:
		d.val = 1.0;
		break;
	}
	var->type = VAR;
	push(d);
}

Inst*
code(Inst f)	/* install one instruction or operand */
{
	Inst *oprogp = progp;
	if (progp >= &prog[NPROG])
		execerror("program too big", (char *)0);
	*progp++ = f;
	return oprogp;
}

void
execute(Inst* p)
{
	for (pc = p; *pc != STOP && !returning; )
		(*((++pc)[-1]))();
}


/**
* logstop - Stops logging current session
* \brief:     Stops logging the current session after logging has
*             started with logstart().
* \details:   The logger process gets killed and file descriptors
*             of the main program are being reset to the default.
*/

void
logstop(void)
{
	if (logfp) { 
		printf("Log ends\n"); 
		fflush(stdout);
		fclose(logfp); 
		logfp = NULL;
	}
}	/*>> logstop() <<*/


/**
* logstart - Starts logging current session to the given file
* \brief:     After a call to logstart, anything going to srdout or stderr
*             of the main program will end up also in the given file.
* \details:   A child process is created that acts as the end point of
*             all output. The child gets all output from the main
*             program and excplicitly handles it by writing to stdout, stderr,
*             and a file. The file is opened and closed in every new write.
*             Thus the logging file given is updated constantly.
*             NOTICE: fflush should be used in every print in the main program
*                     to assure that text arrives in the child's end pipes.
* \param[in]: logfilename   Name of log file to start appending to.
* \todo:      Log even input(commands)
* \todo:      Instead of reading char by char from the pipes,
*             read line by line for efficiency
* \todo:      Log file name isn't saved anywhere. It would be nice
*             if logstart without parameters would automatically
*             reopen last log file used.
* \todo:      Time and date stamp could be added on call to logstart.
* \todo:      More error checking
* \todo:      Do something so fflush is not needed all the time. Maybe use a macro for all
*             print* C commands or change the pipe handling?
*/

void
logstart(char *logfilename)
{
	if (!logfilename || !*logfilename) { 
		warning("Missing log file name argument", NULL); 
		return; 
	} 

	logstop(); 

	if ((logfp = fopen(logfilename, "a+")) == NULL) { 
		warning("Failed to create logfile", logfilename); 
		return; 
	}

	puts("Logging started");
	fflush(stdout);
	return;
}	/*>> logstart() <<*/

/**
* help - diplays contents of a help file(if exists)
* \brief:     Displays help texts about functions on the screen
* \details:   Function attempts to open files from the "help" folder
*	      If the requested help file does not exist an error message
*	      is shown. Help files should be placed in the folder "help" 
*             and the name should be "functionname.hlp". For example the
*	      help file for the help command would be "help/help.hlp".
*	      The help file should have the name of the function of in
*	      the first row and the description on the following rows.
* \todo:      Additional help files for all available functions
*	      should be written.
*/

void
help(char *commandname)			/* Print help file*/
{	
	
	char fullpathname[64];
        sprintf(fullpathname, "help/%s%s", commandname, ".hlp");
	FILE *file = fopen(fullpathname, "r");
	if (file != NULL)
	{
		char line[256];
		int count = 0;
		while ( fgets ( line, sizeof line, file ) != NULL ) /* Read a line */
		{
			if (count == 0)
			{
				fputs ( "Name of function:\n", stdout ); 
				fputs ( line, stdout ); /* Write the line */
				fputs ( "\nDescription:\n", stdout ); 
			}
			else
			{
				fputs ( line, stdout ); /* Write the line */
			}
			count++;
		}
		fputs ( "\n", stdout ); 
	}
	else
	{
		printf("No help entry existing for the requested command\n");
	}
}	/*>> help() <<*/


/**
* Exit - Exits from hoc with exitcode
* \brief:     	This command will exit the live interpreter session or exit a scripted session.
* \details:	When called the hoc program is terminated and returning exitcode to the calling process.
* \todo:	Change the function declaration to "void Exit(int)" and modify the parser code to allow this.
*/

int
Exit(int exitcode)
{
	//Stop logging if active
	logstop();
	//used to exit with exitcode
	exit(exitcode);
	return 0;
}


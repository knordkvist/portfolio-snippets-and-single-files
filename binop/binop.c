/**
 * @brief Calculates the probability that an event with two outcomes and the
 * probability p will occur r times in n trials.
 *
 * In a binomial experiment there are two mutually exclusive outcomes,
 * usually thought of as either "success" or "failure".
 * Two examples are flipping a coin (heads is success and tails is failure, or
 * the other way around) and rolling a die (5 is success and everything else is
 * failure).
 *
 * When computing binomial probability you need to know the probability that
 * the event will occur (p), the number of trials (n) and the number of times
 * you wish the event to occur (r).
 * 
 * @param argc	The size of args.
 *
 * @param args	Contains the three values needed to calculate the probability.
 *		The first element represent the probability that
 * 		the event will occur, the second element represent the number
 *		of trials that will be performed and the third element
 *		represent the number of succesfull trials.
 *
 * @return	The probability that the event will occur.
 */
double binop(int argc, double args[]) {
	if (argc != 3) {
		execerror("binop","Takes 3 arguments");
		return -1;
	}
	// Make sure all values are positive.
	for (int i=0; i<argc; ++i) {
		if (args[i] < 0) {
			execerror("binop", "Arguments cannot contain negative "
					"values");
			return -1;
		}
	}
	double p = args[2];
	double n = args[1];
	double r = args[0];

	if (n < 1) {
		execerror("binop","Number of trials need to be greater than 0");
		return -1;
	}

	if(p > 1) {
		execerror("binop", "Probability must be between 0 and 1");
		return -1;

	}
	// Numbers of trials and successfull events need to be whole numbers.
	if (n != round(n) || r != round(r)) {
		execerror("binop", "Trials and successfull events need to be "
				"whole numbers");
		return -1;
	}
	double probability = 0;
	double combinations = 1;
	/*
	 * The number of possible combinations using r elements
	 * in a set with n elements can be calculated using the following:
	 *
	 * ( n )   (n-0)   (n-1)   (n-2)         (n-(r-1))
	 * (   ) = ----- x ----- x ----- x ... x ---------
	 * ( r )     1       2       3               r
	 */
	for(int i=0; i<=r-1; ++i) {
		combinations *= (n-i) / (i+1);
	}
	probability = combinations * Pow(p,r) * Pow(1-p, n-r);
	return probability;
}